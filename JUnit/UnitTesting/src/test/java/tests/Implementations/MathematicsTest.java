package tests.Implementations;

import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import sources.Mathematics;
import tests.Categories.SlowTests;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Category(SlowTests.class)
@RunWith(Parameterized.class)
public class MathematicsTest {

    public static Mathematics mathematics;

    @Parameterized.Parameters
    public static Collection<Object[]> data()
    {
        return Arrays.asList(new Object[][] {
                { 0, 0 }, { 1, 1 }, { 2, 1 }, { 3, 2 }, { 4, 3 }, { 5, 5 }, { 6, 8 }
        });
    }

    private int _x;
    private int _y;

    public MathematicsTest(int x, int y) {
        _x = x;
        _y = y;
    }


    @BeforeClass
    public static void setDataForClass()
    {
        mathematics = new Mathematics();
    }

    @Before
    public void dropData()
    {
        mathematics.setResult(0);
    }



    @Test(timeout=100)
    public void testForLongTimeExecution() throws Exception
    {
        TimeUnit.SECONDS.sleep(100);
    }

    @Test
    public void add() throws Exception {
        int expected = _x + _y;
        mathematics.add(_x, _y);
        int actual = mathematics.getResult();
        Assert.assertEquals("result does not match expected", expected, actual);
    }

    @Test
    public void deduct() throws Exception {
        int expected = _x - _y;
        mathematics.deduct(_x, _y);
        int actual = mathematics.getResult();
        Assert.assertEquals("result does not match expected", expected, actual);
    }

    @Test
    public void multiply() throws Exception {
        int expected = _x * _y;
        mathematics.multiply(_x, _y);
        int actual = mathematics.getResult();
        Assert.assertEquals("result does not match expected", expected, actual);
    }

    @Test(expected = ArithmeticException.class)
    public void divide() throws Exception {
        int expected = _x / _y;
        mathematics.divide(_x, _y);
        int actual = mathematics.getResult();
        Assert.assertEquals("result does not match expected", expected, actual);
    }

    @Test
    public void getResult() throws Exception {
        int expected = 0;
        Assert.assertEquals("result does not match expected", expected, mathematics.getResult());
    }

    @Test
    public void setResult() throws Exception {
        mathematics.setResult(5);
        int expected = 5;
        Assert.assertEquals("result does not match expected", expected, mathematics.getResult());
    }

}