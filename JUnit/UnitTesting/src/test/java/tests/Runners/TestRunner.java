package tests.Runners;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.Categories.FastTests;
import tests.Categories.SlowTests;
import tests.Implementations.MathematicsTest;
import tests.Implementations.MathematicsTest2;

/**
 * Created by IlyaMakarov on 12/13/2016.
 */

// Exclude Fast category from test run, observe results
/*
@RunWith(Categories.class)
@Categories.IncludeCategory({SlowTests.class})
@Suite.SuiteClasses( { MathematicsTest.class, MathematicsTest2.class })
public class TestRunner {
}
*/

// Create test runner class, include to test run both categories, observe results
@RunWith(Categories.class)
@Categories.IncludeCategory({SlowTests.class, FastTests.class})
@Suite.SuiteClasses( { MathematicsTest.class, MathematicsTest2.class })
public class TestRunner
{

}
